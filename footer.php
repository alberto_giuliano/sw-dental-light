<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SWDental_Light
 */

?>

<footer id="colophon" class="site-footer">
<div class="footer">
	<?php
	wp_nav_menu(
		array(
			'theme_location' => 'menu-2',
			'menu_id'        => 'footer-menu',
		)
	);
	?>
</div>

<div class="container-cta">
	<div class="container-box-cta">
		
	</div>
</div>

<div class="container-footer">
    <div class="container-box-footer">
        <div class="box-footer">
			<h3>Contatti</h3>
            <p>aaa</p>
        </div>
        <div class="box-footer">
			<h3>Link Utili</h3>
            <p><?php
	wp_nav_menu(
		array(
			'theme_location' => 'menu-2',
			'menu_id'        => 'footer-menu',
		)
	);
	?></p>
        </div>
        <div class="box-footer">
            <h3>Le Ultime News</h3>
            <p>aaa</p>
        </div>
        <div class="box-footer">
            <h3>Servizi</h3>
            <p>aaa<br />bbb<br />ccc</p>
        </div>
    </div>
</div>

		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'swdental-light' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'swdental-light' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'swdental-light' ), 'swdental-light', '<a href="https://sw-web.it/">SW web</a>' );
				?>
		</div><!-- .site-info -->

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
