<?php
get_header();
?>

	<main id="primary" class="site-main">

  <div class="post-list">
    <h1 class="post-title">IL NOSTRO TEAM</h1>
		<?php
		while ( have_posts() ) :
			the_post();

      the_title( '<h2>', '</h2>' );
      the_content();
      the_post_thumbnail('team'); 

			//get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>
  </div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
