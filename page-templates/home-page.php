<?php
/**
* Template for displaying Slider with Post
*
* @package swdental-light
*/
/*

Template Name: Home Page

*/
?>
<?php get_header(); ?>


<?php
  $image_url =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'nakedpress_big' );
?>

<!--
/* ! SEZIONE HERO */
-->
<div class="hero" style="background: url(<?php echo $image_url[0]?>); no-repeat center center; background-size: cover;">
    <div class="hero-text">
        <p><?php the_field('intro') ?></p>
        <h1 class="headline"><?php the_field('payoff') ?></h1>
        <button class="services"><a href="<?php the_field('cta_link') ?>"><?php the_field('cta_text') ?></a></button>
    </div>
</div>

<!--
/* ! SEZIONE BOX SERVIZI #1 */
-->
<div class="container-box">
    <div class="container-box-info">
        <img src="<?php the_field('img_boxservizi1') ?>" style="width:100%">
        <div>
            <h5><?php the_field('titolo1_boxservizi1') ?></h5>
            <h2><?php the_field('titolo2_boxservizi1') ?></h2>
            <p><?php the_field('testo_boxservizi1') ?></p>
			<button class="button-empty"><a href="<?php the_field('cta_link_boxservizi1') ?>"><?php the_field('cta_text_boxservizi1') ?></a></button>
        </div>
    </div>
</div>

<!--
/* ! SEZIONE BOX SERVIZI #2 */
-->
<div class="container-box">
    <div class="container-box-services" style="background-color: <?php the_field('bgcolor_boxservizi2') ?>;">
        <div class="container-box-services-text">
            <h5><?php the_field('titolo1_boxservizi2') ?></h5>
            <h2><?php the_field('titolo2_boxservizi2') ?></h2>
            <p><?php the_field('testo_boxservizi2') ?></p>
        </div>
        <div class="container-box-services-img">
            <img src="<?php the_field('img_boxservizi2') ?>" class="service-img">
        </div>
    </div>
</div>

<!--
/* ! SEZIONE BOX SERVIZI #3 */
-->
<div class="container-box">
    <div class="container-box-services-list">
        <div class="service-card">
            <img src="<?php the_field('img1_boxservizi3') ?>">
            <h3><?php the_field('titolo1_boxservizi3') ?></h3>
            <p><?php the_field('txt1_boxservizi3') ?></p>
        </div>
        <div class="service-card">
			<img src="<?php the_field('img2_boxservizi3') ?>">
            <h3><?php the_field('titolo2_boxservizi3') ?></h3>
            <p><?php the_field('txt2_boxservizi3') ?></p>
        </div>
        <div class="service-card">
            <img src="<?php the_field('img3_boxservizi3') ?>">
            <h3><?php the_field('titolo3_boxservizi3') ?></h3>
            <p><?php the_field('txt3_boxservizi3') ?></p>
        </div>
        <div class="service-card">
            <img src="<?php the_field('img4_boxservizi3') ?>">
            <h3><?php the_field('titolo4_boxservizi3') ?></h3>
            <p><?php the_field('txt4_boxservizi3') ?></p>
        </div>
    </div>
</div>

<div class="services-button">
    <button class="services"><a href="<?php the_field('cta_link_boxservizi3') ?>"><?php the_field('cta_text_boxservizi3') ?></a></button>
</div>

<!--
/* ! SEZIONE TEAM */
-->
<div class="container-box">
    <div class="container-box-team">
    <?php
    $obj = get_post_type_object( 'team' );
    echo "<h5>Professionisti al tuo servizio</h2>";
    echo "<h2>".esc_html( $obj->description )."</h2>";
    ?>

    <div class="container-box-team-list">

      <?php
      $args = array(
          'post_type' => 'team',
          'posts_per_page' => 3
      );
      $loop = new WP_Query( $args ); ?>

      <?php if ( $loop->have_posts() ) :

          while ( $loop->have_posts() ) : $loop->the_post();
              ?>
              <div class="team-card">
                <a href="<?php the_permalink(); ?>" class="team-card-link">
                <?php the_post_thumbnail('team', array('class' => 'team-img'));?>
                <h2 class="title"><?php the_title(); ?></h2>
                <h5><?php the_field('team_ruolo'); ?></h5>
                <div class="team-social-box">
                    <a href="<?php the_field('team_linkedin'); ?>" target="_blank" rel="nofollow">
                      <img src="<?php the_field('team_linkedin_icon'); ?>" class="team__icon"></a>
                    <a href="<?php the_field('team_facebook'); ?>" target="_blank" rel="nofollow">
                      <img src="<?php the_field('team_facebook_icon'); ?>" class="team__icon"></a>
                    <a href="<?php the_field('team_instagram'); ?>" target="_blank" rel="nofollow">
                      <img src="<?php the_field('team_instafram_icon'); ?>" class="team__icon"></a>
                    <a mailto="<?php the_field('team_mail'); ?>">
                      <img src="<?php the_field('team_mail_icon'); ?>" class="team__icon"></a>
                </div>
                <?php
                //the_title( '<h2>', '</h2>' );
                //the_content();
                ?>
                </a>
              </div>
              <?php
          endwhile;

          wp_reset_postdata();

        endif; ?>

      </div>
  </div>
</div>

<!--
/* ! SEZIONE MAPPA - CONTATTI */
-->
<div class="container-box">
    <div class="container-box-maps" style="background-color: <?php the_field('bgcolor_boxservizi2') ?>;">
        <div class="container-box-mappa" id="map"></div>
        <div class="container-box-contatti">
            <h5>CONTATTACI</h5>
            <h3 class="title--form">Hai domande? Mettiti in contatto!</h3>
            <?php echo do_shortcode('[contact-form-7 id="89" title="ModuloHome"]'); ?>
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11192.30819521336!2d10.9406661!3d45.4682518!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7e023e2f43486904!2sPNG%20SRL!5e0!3m2!1sen!2sit!4v1648538504225!5m2!1sen!2sit" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->
        </div>
    </div>
</div>

<!--
/* ! SEZIONE FOTO */
-->
<div class="container-box">
    <div class="container-box-foto">
        <div class="foto-card foto-txt">
            <h5><?php the_field('titolo1_boxfoto') ?></h5>
			<h2><?php the_field('titolo2_boxfoto') ?></h2>
            <p><?php the_field('txt_boxfoto') ?></p>
        </div>
        <div class="foto-card">
			<img src="<?php the_field('img1_boxfoto') ?>">
        </div>
        <div class="foto-card">
            <img src="<?php the_field('img2_boxfoto') ?>">
        </div>
        <div class="foto-card">
            <img src="<?php the_field('img3_boxfoto') ?>">
        </div>
		<div class="foto-card">
            <img src="<?php the_field('img4_boxfoto') ?>">
        </div>
		<div class="foto-card">
            <img src="<?php the_field('img5_boxfoto') ?>">
        </div>
    </div>
</div>

<!--
/* ! SEZIONE NEWS */
-->
<div class="container-box">
    <div class="container-box-news">
    <?php
    //$obj = get_post_type_object( 'post' );
    $obj = get_category( 4 );
    echo "<h5>News</h2>";
    echo "<h2 class='title'>".esc_html( $obj->description )."</h2>";
    ?>

    <div class="container-box-news-list">

      <?php
      $args_news = array(
          'post_type' => 'post',
          'post_status' => 'publish',
          'category_name' => 'home-page-post',
          'posts_per_page' => 3
      );
      $loop_news = new WP_Query( $args_news );?>

      <?php if ( $loop_news->have_posts() ) :
          while ( $loop_news->have_posts() ) : $loop_news->the_post();
              ?>
              <article class="news-card <?php post_class();?>'">
                <!--<a href="<?php the_permalink(); ?>">-->
                <?php the_post_thumbnail('swdental-light_single', array('class' => 'news-img', 'alt' => get_the_title() ));?>
                <h2><?php the_title(); ?></h2>
                <p class="news-card-info"><?php the_time( 'd/m/Y' );?></p>
                <p><?php the_excerpt(); ?></p>
                <p class="news-card-info">
                  <a href="<?php the_permalink(); ?>">Continua a leggere</a>
                </p>
              </article>
            <?php endwhile; else: ?>
            <p><?php esc_html_e('Sorry, no post find', 'swdental-light'); ?></p>
            <?php
              wp_reset_postdata();
              endif;
            ?>

      </div>
  </div>
</div>



<?php if (have_posts()) :?>
  <?php while(have_posts()) : the_post(); ?>


    <?php the_content();?>



  <?php endwhile; ?>
<?php else : ?>

  <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'nakedpress'); ?></p>

<?php endif; ?>



<?php get_footer(); ?>
