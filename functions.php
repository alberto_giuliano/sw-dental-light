<?php
/**
 * SWDental Light functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SWDental_Light
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function swdental_light_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on SWDental Light, use a find and replace
		* to change 'swdental-light' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'swdental-light', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	add_image_size('swdental-light_single', 600, 400, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Header', 'swdental-light' ),
			'menu-2' => esc_html__( 'Footer', 'swdental-light' ),
		)
	);

	//Disable emojis in WordPress
	add_action( 'init', 'smartwp_disable_emojis' );

	function smartwp_disable_emojis() {
	  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	  remove_action( 'wp_print_styles', 'print_emoji_styles' );
	  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	  remove_action( 'admin_print_styles', 'print_emoji_styles' );
	  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	}

	function disable_emojis_tinymce( $plugins ) {
	  if ( is_array( $plugins ) ) {
	    return array_diff( $plugins, array( 'wpemoji' ) );
	  } else {
	    return array();
	  }
	}

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'swdental_light_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'swdental_light_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function swdental_light_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'swdental_light_content_width', 640 );
}
add_action( 'after_setup_theme', 'swdental_light_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function swdental_light_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'swdental-light' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'swdental-light' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'swdental_light_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function swdental_light_scripts() {
	wp_enqueue_style( 'swdental-light-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'swdental-light-style', 'rtl', 'replace' );

	wp_enqueue_script( 'swdental-light-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	wp_enqueue_script( 'swdental-light-maps', get_template_directory_uri() . '/js/maps.js', array(), null, true );

	wp_enqueue_script( 'swdental-light-mapsapi', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAT-B-eA36Yci0ILRWir6OM37ZRwfjbZto&callback=initMap', array(), _S_VERSION, true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'swdental_light_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
* Remove WP version in source page
*/
remove_action('wp_head', 'wp_generator');



/**
* ABILITA CARICAMENTO SVG
*/
function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
* EXCERPT LENGTH
*/
add_filter( 'excerpt_length', function($length) {
    return 15;
}, PHP_INT_MAX );
