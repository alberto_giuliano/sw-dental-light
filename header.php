<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SWDental_Light
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'swdental-light' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<div class="logo">
			<?php
				if ( function_exists( 'the_custom_logo' ) ) {
					the_custom_logo();
				}
			?>
			</div>
			<div class="identity">
				STUDIO DENTISTICO
				<br /><small><?php bloginfo( 'name' ); ?> |
				<?php bloginfo( 'description' ); ?></small></div>
			<nav>
				<ul class="menu">
					<li>
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							)
						);
					?>
					</li>
				</ul>


			</nav>

			<button class="contact">CONTATTACI</button>
			<div class="menu-social">
				<a href="#"><i class="fab fa-facebook-square fa-2x"></i></a>
				<a href="#"><i class="fab fa-linkedin fa-2x"></i></a>
			</div>
		</div><!-- .site-branding -->

	</header><!-- #masthead -->
