function initMap() {
  // Styles a map in night mode.
  const center = { lat: 45.4682518, lng: 10.9406661 };

  const map = new google.maps.Map(document.getElementById("map"), {
    center: center,
    zoom: 15,
    styles: [
      { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
      { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
      { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
      {
        featureType: "administrative.locality",
        elementType: "labels.text.fill",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "poi",
        //elementType: "labels.text.fill",
        stylers: [{ "visibility": "off" }],
      },
      {
        featureType: "poi.park",
        elementType: "geometry",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "poi.park",
        elementType: "labels.text.fill",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "poi.business",
        stylers: [{ "visibility": "off" }],
      },
      {
        featureType: "road",
        elementType: "geometry",
        stylers: [{ color: "#38414e" }],
      },
      {
        featureType: "road",
        elementType: "geometry.stroke",
        stylers: [{ color: "#212a37" }],
      },
      {
        featureType: "road",
        elementType: "labels.text.fill",
        stylers: [{ color: "#9ca5b3" }],
      },
      {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [{ color: "#746855" }],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{ color: "#1f2835" }],
      },
      {
        featureType: "road.highway",
        elementType: "labels.text.fill",
        //stylers: [{ color: "#f3d19c" }],
        stylers: [{ color: "#ffffff" }],
      },
      {
        featureType: "transit",
        elementType: "geometry",
        stylers: [{ color: "#2f3948" }],
      },
      {
        featureType: "transit.station",
        elementType: "labels.text.fill",
        stylers: [{ color: "#d59563" }],
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [{ color: "#17263c" }],
      },
      {
        featureType: "water",
        elementType: "labels.text.fill",
        stylers: [{ color: "#515c6d" }],
      },
      {
        featureType: "water",
        elementType: "labels.text.stroke",
        stylers: [{ color: "#17263c" }],
      },
    ],
  });

  // The marker, positioned at Uluru
 const marker = new google.maps.Marker({
   position: center,
   map: map,
   title: "AAA",
 });

 google.maps.event.addListener(marker, "click", function() {
   infowindow.open(map,marker);
 });

 var contentString =
    '<div class="popup" style="color:#222222">'+
    '<h2 id="berlin">Berlin</h2>'+
    '<p>Center of Berlin</b><br/>'+
    '<small><b>Lat.</b> 52.520196, <b>Lon.</b> 13.406067</small></p>'+
    '<a target="_blank" href="https://maps.google.it/maps?q=Hotel+Berlino,+Via+Giovanni+Antonio+Plana,+Milano,+MI&hl=it&sll=45.086854,9.328594&sspn=0.077082,0.181789&oq=berlino&hq=Hotel+Berlino,+Via+Giovanni+Antonio+Plana,+Milano,+MI&t=m&z=15">'+
    'Watch the Full Screen Map &#187;</a>'+
    '</div>';

  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 230,
      maxHeight: 300,
  });


}
